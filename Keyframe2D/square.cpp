/**
 * cpp file for Square subclass of Geometry
 *
 * Samuel Kenney
 * Grove City College
 * March 17, 2017
 */

#include "square.h"


square::square(unsigned int count) : Geometry() {
	pointCount = count;
	points = new gVector4[pointCount];

	//provide both points for each of the lines needed
	points[0] = gVector4(2,2,0,1);
	points[1] = gVector4(-2,2,0,1);
	points[2] = gVector4(-2,-2,0,1);
	points[3] = gVector4(2,-2,0,1);

	colors = new gVector4[pointCount];
	for(int i = 0; i < pointCount; ++i) {
		//colors[i] = gVector4(255.0f, 0.0f,0.0f, 1.0f);
		colors[i] = gVector4(0.0f, 110.0f, 200.0f, 1.0f);
	}
}

square::~square() {
	delete[] points;
	delete[] colors;
}

void square::draw(const gMatrix4& xform) {
	context->glUniformMatrix4fv(xformMatrixLocation, 1, GL_TRUE, reinterpret_cast<const float*>(&xform));

	context->glBindBuffer(GL_ARRAY_BUFFER, vbo);
	context->glBufferData(GL_ARRAY_BUFFER, pointCount * sizeof(gVector4), points, GL_STATIC_DRAW);

	context->glBindBuffer(GL_ARRAY_BUFFER, cbo);
	context->glBufferData(GL_ARRAY_BUFFER, pointCount * sizeof(gVector4), colors, GL_STATIC_DRAW);

	context->glBindBuffer(GL_ARRAY_BUFFER, vbo);
	context->glEnableVertexAttribArray(vLocation);
	
	context->glVertexAttribPointer(vLocation, 4, GL_FLOAT, GL_FALSE, 0, 0);
	context->glBindBuffer(GL_ARRAY_BUFFER, cbo);
	
	context->glEnableVertexAttribArray(cLocation);
	context->glVertexAttribPointer(cLocation, 4, GL_FLOAT, GL_FALSE, 0, 0);
	
	context->glDrawArrays(GL_POLYGON, 0, pointCount);
}
