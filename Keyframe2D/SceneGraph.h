/**
 * Starter code for Program 3: Milestone 1. This is the header file for the SceneGraph class. Do not change this file, this is 
 * the interface you are required to implement in a .cpp; you do not need to add any new data or functions to do so.
 *
 * Dr. Cory D. Boatright
 * Grove City College
 * March 13, 2017
 */

#pragma once

#include <list>
#include "Geometry.h"
#include "gMatrix4.h"

class SceneGraph {
public:
	//Standard constructor, should create inert transformation variables
	//If the node shouldn't render anything, pass a nullptr as the parameter.
	SceneGraph(Geometry*);

	//Copy constructor, since this is a tree/graph, you need to perform a deep copy
	//The default copy constructor only does a shallow copy
	SceneGraph(const SceneGraph&);
	
	//destructor
	~SceneGraph(void);

	//Add the new scene graph node to the end of the children list
	void addChild(const SceneGraph&);

	//traverse the scene graph as discussed in class
	void traverse(gMatrix4) const;

	//setters and getters
	float getScaleX(void) const;
	void setScaleX(float);
	
	float getScaleY(void) const;
	void setScaleY(float);

	float getTheta(void) const;
	void setTheta(float);
	
	float getTranslateX(void) const;
	void setTranslateX(float);
	
	float getTranslateY(void) const;
	void setTranslateY(float);

private:
	std::list<SceneGraph> children;
	Geometry* geom;
	float theta;
	float scaleFactor[2];
	float translation[2];
};