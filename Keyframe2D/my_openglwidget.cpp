/**
 * Starter code for Program 3: Milestone 1. This is the implementation file for the OpenGL widget and rendering context.
 * This file includes much of the "boilerplate" code to get the rendering context set up.
 *
 * Dr. Cory D. Boatright
 * Grove City College
 * March 13, 2017
 */

#include "my_openglwidget.h"

My_OpenGLWidget::My_OpenGLWidget(QWidget* parent) : QOpenGLWidget(parent) {
	//most pointers are initialized to nullptr for proper programming technique
	vertexShader = nullptr;
	fragmentShader = nullptr;
	shaderProgram = nullptr;

	root = nullptr;
	fusilage = nullptr;
	top = nullptr;
	middle = nullptr;
	bottom = nullptr;
	sign = nullptr;
	wings = nullptr;
	base = nullptr;
	trapBase = nullptr;
	octBase = nullptr;
	cap = nullptr;
	triCap = nullptr;

	//initialize shapes: delete geometry subclass
	//triangle: 3 points
	tri = new triangle(3);
	//square: 4 points
	sqr = new square(4);
	//trapezoid: 4 points
	trap = new trapezoid(4);
	//octagon: 8 points
	oct = new octagon(8);
	//non_convex: 18 points
	noncon = new non_convex(18);
}

My_OpenGLWidget::~My_OpenGLWidget() {
	delete vertexShader;
	delete fragmentShader;
	delete shaderProgram;

	//delete nodes
	delete root;
	delete fusilage;
	delete top;
	delete middle;;
	delete bottom;
	delete sign;
	delete wings;
	delete base;
	delete trapBase;
	delete octBase;
	delete cap;
	delete triCap;

	//shapes: delete geometry subclass
	delete tri;
	delete sqr;
	delete trap;
	delete oct;
	delete noncon;
	
}

//called only once, after the constructor has finished running but before the first paintGL or resizeGL call
void My_OpenGLWidget::initializeGL() {
	//basically connects to the graphics driver without the typical mess
	initializeOpenGLFunctions();
	//by setting up a pointer in geometry to "this," we can ensure OpenGL function calls apply to this view
	Geometry::context = this;		//let the Geometry nodes render based on this context

	//background color is black
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	
	//skipping the depth testing since we are using 2D graphics, all the depths would be the same
	//may need to have scene graph nodes render after their recursive call for proper layering effects

	//allocate shaders and shader program
	vertexShader = new QOpenGLShader(QOpenGLShader::Vertex, this);
	fragmentShader = new QOpenGLShader(QOpenGLShader::Fragment, this);
	shaderProgram = new QOpenGLShaderProgram(this);

	//compile and link the shaders, the shaders have not been provided with the starter code
	vertexShader->compileSourceFile("simple_xform_color.vert");
	fragmentShader->compileSourceFile("pass_through.frag");
	//check the Qt 5.5.1 API for how you can view GLSL compiler and linker errors
	shaderProgram->addShader(vertexShader);
	shaderProgram->addShader(fragmentShader);
	shaderProgram->link();
	
	//set the shader program for use
	shaderProgram->bind();

	//get attribute and uniform locations, this connects data to variables in the shaders
	Geometry::vLocation = shaderProgram->attributeLocation("vs_position");
	Geometry::cLocation = shaderProgram->attributeLocation("vs_color");
	Geometry::xformMatrixLocation = shaderProgram->uniformLocation("u_xform");

	//view matrix is handled special, you do not need to change anything about the view matrix
	//note that you need to continue to use the same variable name inside your vertex shader
	viewMatrixLocation = shaderProgram->uniformLocation("u_view");

	//it's wasteful but we'll use two vertex buffer objects for our renderings
	glGenBuffers(1, &Geometry::vbo);
	glGenBuffers(1, &Geometry::cbo);

	//for this milestone, use this function to build the scene graph
	buildGraph();
}

void My_OpenGLWidget::buildGraph() {
	//create an interesting scene/figure
	root = new SceneGraph(nullptr);
		fusilage = new SceneGraph(nullptr);
		fusilage->setTranslateY(-2.0f);
			top = new SceneGraph(sqr);
			top->setTranslateY(4.0f);
			middle = new SceneGraph(sqr);
			middle->setTranslateY(2.5f);
			bottom = new SceneGraph(sqr);
			bottom->setTranslateY(0.5f);
			sign = new SceneGraph(noncon);
			sign->setTranslateY(1.7f);
			sign->setScaleX(0.4f);
			sign->setScaleY(0.4f);
			wings = new SceneGraph(trap);
			wings->setTranslateY(4.5f);
			wings->setScaleX(0.5f);
			wings->setScaleY(0.5f);

		base = new SceneGraph(nullptr);
		base->setTranslateY(-4.7f);
			trapBase = new SceneGraph(trap);
			trapBase->setScaleX(0.7f);
			trapBase->setScaleY(0.7f);
			trapBase->setTranslateY(-1.0f);
			octBase = new SceneGraph(oct);
			octBase->setScaleX(0.5f);
			octBase->setScaleY(0.6f);

		cap = new SceneGraph(nullptr);
		cap->setTranslateY(6.9f);
		cap->setScaleX(0.7f);
			triCap = new SceneGraph(tri);


	cap->addChild(*triCap);

	base->addChild(*trapBase);
	base->addChild(*octBase);

	fusilage->addChild(*wings);
	fusilage->addChild(*bottom);
	fusilage->addChild(*middle);
	fusilage->addChild(*top);
	fusilage->addChild(*sign);

	root->addChild(*cap);
	root->addChild(*base);
	root->addChild(*fusilage);
	
}

//called any time Qt needs to refresh the window that contains this widget
//is NOT set to any sort of framerates
void My_OpenGLWidget::paintGL() {
	//clear first to reset to the background color
	glClear(GL_COLOR_BUFFER_BIT);

	//traverse scene graph, pass the identity matrix as our starting point
	root->traverse(gMatrix4::identity());

	//draw shapes: subclass of geometry
	//triangle
	//tri->draw(gMatrix4::identity());
	//square
	//sqr->draw(gMatrix4::identity());
	//trapezoid
	//trap->draw(gMatrix4::identity());
	//octagon
	//oct->draw(gMatrix4::identity());
	//non_convex
	//noncon->draw(gMatrix4::identity());

	//flush is a blocking call that ensures the renderings are all finished and displayed before moving on
	glFlush();
}

//called anytime the widget is resized (we have that disabled), the initial creation of the widget counts as a "resize"
void My_OpenGLWidget::resizeGL(int width, int height) {
	//sets the window for the rendering context, leave this along
	glViewport(0, 0, width, height);
	//hardcoded orthographic projection; this makes the widget cover the space from (-10,-10) to (10,10)
	gMatrix4 ortho(gVector4(1.0f / 10.0f, 0.0f, 0.0f, 0.0f),
				   gVector4(0.0f, 1.0f / 10.0f, 0.0f, 0.0f),
				   gVector4(0.0f, 0.0f, 1.0f, 0.0f),
				   gVector4(0.0f, 0.0f, 0.0f, 1.0f));
	glUniformMatrix4fv(viewMatrixLocation, 1, GL_TRUE, reinterpret_cast<float*>(&ortho));
}