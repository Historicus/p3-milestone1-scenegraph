/**
 * Starter code for Program 3: Milestone 1. This is the header file for the OpenGL widget and rendering context.
 * Necessary changes to this file should be minimal to complete the assignment. Excessive editing can cause problems
 * when moving to the next milestone.
 *
 * Dr. Cory D. Boatright
 * Grove City College
 * March 13, 2017
 */

#pragma once

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLShader>
#include "gVector4.h"
#include "gMatrix4.h"
#include "TestRender.h"
#include "SceneGraph.h"

//shapes: subclass of geometry
#include "triangle.h"
#include "square.h"
#include "trapezoid.h"
#include "octagon.h"
#include "non_convex.h"

//one of the few examples of beneficial multiple inheritance, since this merges the rendering context
//with the viewport itself
class My_OpenGLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
	Q_OBJECT

public:
	My_OpenGLWidget(QWidget* parent = nullptr);
	~My_OpenGLWidget(void);

	//necessary functions which are automatically called by Qt as needed to display the window and thus, the
	//OpenGL rendering
	void initializeGL(void);
	void paintGL(void);
	void resizeGL(int, int);

private:
	//helper function for building the graph itself
	void buildGraph(void);

	//boilerplate variables
	QOpenGLShader* vertexShader;
	QOpenGLShader* fragmentShader;
	QOpenGLShaderProgram* shaderProgram;
	unsigned int viewMatrixLocation;
	
	//the pointer that will be used as the root of your scene graph
	SceneGraph* root;
		SceneGraph* fusilage;
			SceneGraph* top;
			SceneGraph* middle;
			SceneGraph* bottom;
			SceneGraph* sign;
			SceneGraph* wings;
		SceneGraph* base;
			SceneGraph* trapBase;
			SceneGraph* octBase;
		SceneGraph* cap;
			SceneGraph* triCap;

	//shapes: subclass of geometry
	//triangle
	triangle* tri;
	//square
	square* sqr;
	//trapezoid
	trapezoid* trap;
	//octagon
	octagon* oct;
	//non-convex
	non_convex* noncon;
};