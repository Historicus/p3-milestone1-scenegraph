/**
 * Starter code for Program 3: Milestone 1. This is the header file for the example geometry node "TestRender."
 * It serves to show how to inherit from the Geometry class, create data, and then use Geometry's static members
 * to render a scene in the OpenGL widget. Since it is an example, this file should not need to be changed.
 *
 * Dr. Cory D. Boatright
 * Grove City College
 * March 13, 2017
 */

#pragma once

#include "Geometry.h"
#include "gVector4.h"

class TestRender : public Geometry {
public:
	TestRender(unsigned int);
	~TestRender(void);
	void draw(const gMatrix4&);

private:
	gVector4* points;
	gVector4* colors;
	unsigned int pointCount;
};

