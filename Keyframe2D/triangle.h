/**
 * Header for Triangle subclass of Geometry
 *
 * Samuel Kenney
 * Grove City College
 * March 17, 2017
 */

#pragma once
#include "Geometry.h"
#include "gVector4.h"

class triangle : public Geometry
{
public:
	triangle(unsigned int);
	~triangle(void);
	void draw(const gMatrix4&);

private:
	gVector4* points;
	gVector4* colors;
	unsigned int pointCount;
};

