/**
 * Starter code for Program 3: Milestone 1. This is the header file for the abstract class Geometry. All
 * geometry nodes should inherit from this class and implement the draw function. The OpenGL widget will
 * instantiate the static variables, which can then be used to coordinate OpenGL function calls across many
 * objects. This file should not need to be changed.
 *
 * Dr. Cory D. Boatright
 * Grove City College
 * March 13, 2017
 */

#pragma once

#include "gMatrix4.h"
#include <QOpenGLFunctions>

class Geometry {
	friend class My_OpenGLWidget;
public:
	Geometry(void);
	virtual ~Geometry(void);
	virtual void draw(const gMatrix4&) = 0;

protected:
	static unsigned int vLocation;
	static unsigned int cLocation;
	static unsigned int xformMatrixLocation;
	static unsigned int vbo;
	static unsigned int cbo;
	static QOpenGLFunctions* context;
};