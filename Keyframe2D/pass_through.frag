#version 130

in vec4 fs_color;

out vec4 out_Color;

void main() {	
  out_Color = fs_color;
}