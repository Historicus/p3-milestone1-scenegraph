#version 130

uniform mat4 u_view;
uniform mat4 u_xform;

in vec4 vs_position;
in vec4 vs_color;

out vec4 fs_color;

void main() {	
  fs_color = vs_color;
  gl_Position = u_view * u_xform * vs_position;
}