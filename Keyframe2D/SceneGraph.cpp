#include "SceneGraph.h"

//Standard constructor, should create inert transformation variables
//If the node shouldn't render anything, pass a nullptr as the parameter.
SceneGraph::SceneGraph(Geometry* node){
	children.clear();
	geom = node;
	theta = 0.0f, scaleFactor[0] = 1.0f, scaleFactor[1] = 1.0f, translation[0] = 0.0f, translation[1] = 0.0f;

}

//Copy constructor, since this is a tree/graph, you need to perform a deep copy
//The default copy constructor only does a shallow copy
SceneGraph::SceneGraph(const SceneGraph& sg){
	geom = sg.geom;

	theta = sg.theta;
	scaleFactor[0] = sg.scaleFactor[0];
	scaleFactor[1] = sg.scaleFactor[1];
	translation[0] = sg.translation[0];
	translation[1] = sg.translation[1];

	children = sg.children;

}

//destructor
SceneGraph::~SceneGraph(void){
}

//Add the new scene graph node to the end of the children list
void SceneGraph::addChild(const SceneGraph& node){
	children.push_back(node);
}

//traverse the scene graph as discussed in class
void SceneGraph::traverse(gMatrix4 T) const{

	//1. "Do what I need to do"
	//	a. Multiply my transformations with the passed in matrix
	gMatrix4 scaleT = gMatrix4::scale3D(getScaleX(), getScaleY(), 0.0f);

	gMatrix4 rotateZ = gMatrix4::rotateZ(theta);

	gMatrix4 transT = gMatrix4::translate3D(getTranslateX(), getTranslateY(), 0.0f);

	gMatrix4 newT =	T * (transT * rotateZ * scaleT );

	//	b. Draw geometry if present, i.e., if ptr != nullptr
	if (geom != nullptr){
		geom->draw(newT);
	}

	//2. "Recurse over my children"
	std::list<SceneGraph>::const_iterator iterator;
	for (iterator = children.begin(); iterator != children.end(); ++iterator) {
		//traverse each child
		iterator->traverse(newT);
	}

}

//setters and getters
float SceneGraph::getScaleX(void) const{return scaleFactor[0];}
void SceneGraph::setScaleX(float scaleX){scaleFactor[0] = scaleX;}

float SceneGraph::getScaleY(void) const{return scaleFactor[1];}
void SceneGraph::setScaleY(float scaleY){scaleFactor[1] = scaleY;}

float SceneGraph::getTheta(void) const{return theta;}
void SceneGraph::setTheta(float angle){theta = angle;}

float SceneGraph::getTranslateX(void) const{return translation[0];}
void SceneGraph::setTranslateX(float transX){translation[0] = transX;}

float SceneGraph::getTranslateY(void) const{return translation[1];}
void SceneGraph::setTranslateY(float transY){translation[1] = transY;}
