/**
 * Starter code for Program 3: Milestone 1. This is the implementation file for the example geometry node "TestRender."
 * It serves to show how to inherit from the Geometry class, create data, and then use Geometry's static members
 * to render a scene in the OpenGL widget. Since it is an example, this file should not need to be changed.
 *
 * Dr. Cory D. Boatright
 * Grove City College
 * March 13, 2017
 */

#include "TestRender.h"

TestRender::TestRender(unsigned int count) : Geometry() {
	pointCount = count;
	points = new gVector4[pointCount];

	gVector4 vertices[3] = {gVector4(-10.0f, -10.0f, 0.0f, 1.0f), gVector4(0.0f, 10.0f, 0.0f, 1.0f), gVector4(10.0f, -10.0f, 0.0f, 1.0f)};
	points[0] = gVector4(2.5f, 5.0f, 0.0f, 1.0f);

	for(int i = 1; i < pointCount; i++) {
		int k = rand() % 3;
		points[i] = gVector4((points[i-1][0] + vertices[k][0]) / 2.0f, (points[i-1][1] + vertices[k][1]) / 2.0f, 0.0f, 1.0f);
	}

	colors = new gVector4[pointCount];
	for(int i = 0; i < pointCount; ++i) {
		colors[i] = gVector4(static_cast<float>(rand() % 256) / 255.0f, static_cast<float>(rand() % 256) / 255.0f, static_cast<float>(rand() % 256) / 255.0f, 1.0f);
	}
}

TestRender::~TestRender() {
	delete[] points;
	delete[] colors;
}

void TestRender::draw(const gMatrix4& xform) {
	context->glUniformMatrix4fv(xformMatrixLocation, 1, GL_TRUE, reinterpret_cast<const float*>(&xform));

	context->glBindBuffer(GL_ARRAY_BUFFER, vbo);
	context->glBufferData(GL_ARRAY_BUFFER, pointCount * sizeof(gVector4), points, GL_STATIC_DRAW);

	context->glBindBuffer(GL_ARRAY_BUFFER, cbo);
	context->glBufferData(GL_ARRAY_BUFFER, pointCount * sizeof(gVector4), colors, GL_STATIC_DRAW);

	context->glBindBuffer(GL_ARRAY_BUFFER, vbo);
	context->glEnableVertexAttribArray(vLocation);
	
	context->glVertexAttribPointer(vLocation, 4, GL_FLOAT, GL_FALSE, 0, 0);
	context->glBindBuffer(GL_ARRAY_BUFFER, cbo);
	
	context->glEnableVertexAttribArray(cLocation);
	context->glVertexAttribPointer(cLocation, 4, GL_FLOAT, GL_FALSE, 0, 0);
	
	context->glDrawArrays(GL_POINTS, 0, pointCount);
}
