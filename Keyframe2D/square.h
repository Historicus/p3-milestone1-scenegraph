/**
 * Header for Square subclass of Geometry
 *
 * Samuel Kenney
 * Grove City College
 * March 17, 2017
 */

#pragma once
#include "Geometry.h"
#include "gVector4.h"

class square : public Geometry
{
public:
	square(unsigned int);
	~square(void);
	void draw(const gMatrix4&);

private:
	gVector4* points;
	gVector4* colors;
	unsigned int pointCount;
};

