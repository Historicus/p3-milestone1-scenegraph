/**
 * cpp file for Triangle subclass of Geometry
 *
 * Samuel Kenney
 * Grove City College
 * March 17, 2017
 */

#include "triangle.h"


triangle::triangle(unsigned int count) : Geometry() {
	pointCount = count;
	points = new gVector4[pointCount];

	//provide both points for each of the lines needed
	points[0] = gVector4(0,2,0,1);
	points[1] = gVector4(-2.88,-3,0,1);
	points[2] = gVector4(2.88,-3,0,1);

	colors = new gVector4[pointCount];
	for(int i = 0; i < pointCount; ++i) {
		//colors[i] = gVector4(255.0f, 0.0f,0.0f, 1.0f);
		colors[i] = gVector4(150.0f, 100.0f, 50.0f, 1.0f);
	}
}

triangle::~triangle() {
	delete[] points;
	delete[] colors;
}

void triangle::draw(const gMatrix4& xform) {
	context->glUniformMatrix4fv(xformMatrixLocation, 1, GL_TRUE, reinterpret_cast<const float*>(&xform));

	context->glBindBuffer(GL_ARRAY_BUFFER, vbo);
	context->glBufferData(GL_ARRAY_BUFFER, pointCount * sizeof(gVector4), points, GL_STATIC_DRAW);

	context->glBindBuffer(GL_ARRAY_BUFFER, cbo);
	context->glBufferData(GL_ARRAY_BUFFER, pointCount * sizeof(gVector4), colors, GL_STATIC_DRAW);

	context->glBindBuffer(GL_ARRAY_BUFFER, vbo);
	context->glEnableVertexAttribArray(vLocation);
	
	context->glVertexAttribPointer(vLocation, 4, GL_FLOAT, GL_FALSE, 0, 0);
	context->glBindBuffer(GL_ARRAY_BUFFER, cbo);
	
	context->glEnableVertexAttribArray(cLocation);
	context->glVertexAttribPointer(cLocation, 4, GL_FLOAT, GL_FALSE, 0, 0);
	
	context->glDrawArrays(GL_TRIANGLES, 0, pointCount);
}
